# ![Logo](chrome/app/theme/chromium/product_logo_64.png) Carbon by Questbook

Carbon is an open-source browser project based on Chromium that aims to build a safer, faster,
and more simplistic way for all users to experience the web.

# For developers and testers only.
To build Carbon, check out the system requirements below:

## System Requirements
* 100GB of free storage left (256GB recommended)
* 8GB ram (16GB recommended)
* Microsoft Windows 1904 or higher (Windows 11 does work)
* Visual Studio Code 2019 (Visual Studio Code 2022 will also work)
* git for Windows needs to be downloaded (https://git-scm.com/)

## Building Carbon (Windows only)
You can also read the chromium docs here: (https://chromium.googlesource.com/chromium/src/+/HEAD/docs/windows_build_instructions.md)

* Download depot_tools (https://storage.googleapis.com/chrome-infra/depot_tools.zip).
First you need to edit the environmental variables on to your system:
```Control Panel → System and Security → System → Advanced system settings```
Add``` C:\src\depot_tools``` to the front of your other environment variables. Note: If your system PATH has a Python in it, you will be out of luck.
```set vs2019_install=C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional``` for Visual Studio 2019, or set ```vs2022_install=C:\Program Files\Microsoft Visual Studio\2022\Professional``` for Visual Studio 2022.

### Running and compiling Carbon!
Here are the commands to run and compile Carbon:<br>
```gclient``` is the default command for Carbon, to ensure that this works; please make sure that <br>```python3``` is used to run .py files.<br>
```autoninja``` Compiler for building Carbon.<br>


Now that you have the basics down, it's time to compile Carbon!
in your `C:\` drive make a directory for Carbon followed by a `src` folder (Example: ``mkdir web-browser → cd web-browser → mkdir src``, this could be any name you want). 

clone the repo in the `src` folder by typing ```git clone https://gitlab.com/questbook/carbon.git```
extract the ```depot_tools.zip``` into the src folder.
Using the ``fetch`` command should result with depot_tools being updated. 
As soon as it is done updating, use ```gclient sync``` to update the project on to your computer. This will take an hour to finish.

## Compiling Carbon

### MAKE SURE THAT YOU HAVE FOLLOWED ALL THE STEPS BEFORE COMPILING CARBON!
Type ``cd src`` and enter the following code ``gn gen out/Carbon``. once that is complete, enter this command ```autoninja -C out/Default chrome``` This will take an hour to finish.

Once it is done, you can proceed by ```out/Default chrome``` and it should run!


# For non-developers
(windows only) Just head over to questbook.quest and download the .exe setup file.
Linux and MacOS version coming soon.