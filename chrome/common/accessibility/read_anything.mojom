// Copyright 2021 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// A module for a prototype of the Read Anything feature.
module read_anything.mojom;

import "skia/public/mojom/skcolor.mojom";
import "ui/accessibility/mojom/ax_event.mojom";
import "ui/accessibility/mojom/ax_tree_id.mojom";
import "ui/accessibility/mojom/ax_tree_update.mojom";
import "url/mojom/url.mojom";

// Used to represent the current user choices for the Read Anything visual
// presentation/theme. This includes font name, size, spacing, and colors.
struct ReadAnythingTheme {
  // The name of the user's font choice.
  string font_name;

  // The px value of the user's font size.
  float font_size;

  // The various colors of the user's chosen theme.
  skia.mojom.SkColor foreground_color;
  skia.mojom.SkColor background_color;

  // The enum value of the user's line spacing choice
  read_anything.mojom.Spacing line_spacing;

  // The enum value of the user's letter spacing choice.
  read_anything.mojom.Spacing letter_spacing;
};

// Used by the WebUI page to bootstrap bidirectional communication.
interface PageHandlerFactory {
  // The WebUI calls this method when the page is first initialized.
  CreatePageHandler(pending_remote<Page> page,
                    pending_receiver<PageHandler> handler);
};

// Used as identifiers for the Read Anything theme options.
// Next value: 4
[Extensible, Stable, Uuid="03f38cfc-a34f-460a-a0a7-faf711f11a64"]
enum Colors {
  [Default]kDefault = 0,
  kLight = 1,
  kDark = 2,
  kYellow = 3,
};

[Extensible, Stable, Uuid="2B5C793A-D81E-4C76-8CDF-695C7E0A30E2",
 RenamedFrom="read_anything.mojom.LetterSpacing"]
enum Spacing {
  kTight = 0,
  [Default]kDefault = 1,
  kLoose = 2,
  kVeryLoose = 3,
};

// Browser-side handler for requests from WebUI page.
interface PageHandler {
  // Informs the browser controller that a link was clicked in the WebUI. The
  // browser controller responds by opening the url. Url is the URL of the link
  // that was clicked. Open_in_new_tab is true if the link element has the
  // attribute `link = "_blank"`.
  OnLinkClicked(url.mojom.Url url, bool open_in_new_tab);
};

// WebUI-side handler for requests from the browser.
interface Page {
  // Send an accessibility event notification to the WebUI. The WebUI
  // unserializes the updates and stores a copy of the tree with ID tree_id in
  // a map.
  AccessibilityEventReceived(ax.mojom.AXTreeID tree_id,
                             array<ax.mojom.AXTreeUpdate> updates,
                             array<ax.mojom.AXEvent> events);

  // Sends the active AXTreeID to the WebUI. This is not guaranteed to be called
  // after the tree_id was previously passed to AccessibilityEventsReceived.
  OnActiveAXTreeIDChanged(ax.mojom.AXTreeID tree_id);

  // Notifies the WebUI that an AXTree with the provided AXTreeID was
  // destroyed. This is not guaranteed to be called after the tree_id was
  // previously passed to AccessibilityEventsReceived.
  OnAXTreeDestroyed(ax.mojom.AXTreeID tree_id);

  // Send an updated theme to the WebUI.
  OnThemeChanged(ReadAnythingTheme new_theme);
};
