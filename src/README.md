# ![Logo](chrome/app/theme/chromium/product_logo_64.png) Carbon by Questbook

Carbon is an open-source browser project based on Chromioum that aims to build a safer, faster,
and more stable way for all users to experience the web.

# For developers only.
To build Carbon on windows, check out the Chromium docs here:
(https://chromium.googlesource.com/chromium/src/+/main/docs/windows_build_instructions.md)

If you're on linux, check out the Chromium docs here:
(https://chromium.googlesource.com/chromium/src/+/main/docs/linux/build_instructions.md)

# For non-developers
(windows only) Just head over to questbook.quest and download the .exe file