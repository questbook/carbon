// Copyright 2021 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

module blink.mojom;

import "services/device/public/mojom/pressure_update.mojom";

// Implemented by renderers to receive compute pressure info from the browser.
//
interface PressureObserver {
  // Updates to an observer are rate-limited. Observers receive updates at most
  // once per second.
  OnUpdate(device.mojom.PressureUpdate update);
};

// Result of PressureService.BindObserver().
enum PressureStatus {
  kOk,

  // The underlying platform does not report compute pressure information or
  // the renderer is not allowed access to the feature.
  kNotSupported,
};

// The interface is implemented in the browser and consumed by renderers. The
// interface is only accessible to frames (and not workers).  Each frame that
// accesses the API creates a separate connection.
interface PressureService {
  // Subscribes to updates on the device's PressureState.
  //
  // `observer` is active (eligible for notifications) as soon as
  // binding completes. Observation is stopped by
  // disconnecting the mojo pipe.
  BindObserver(pending_remote<PressureObserver> observer)
      => (PressureStatus status);
};
