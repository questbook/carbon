# Copyright 2016 The Carbon Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import unittest

from blinkpy.common.host_mock import MockHost
from blinkpy.common.path_finder import RELATIVE_WPT_TESTS
from blinkpy.common.system.executive import ScriptError
from blinkpy.common.system.executive_mock import MockExecutive, mock_git_commands
from blinkpy.w3c.Carbon_commit import CarbonCommit


class CarbonCommitTest(unittest.TestCase):
    def test_validates_sha(self):
        with self.assertRaises(AssertionError):
            CarbonCommit(MockHost(), sha='rutabaga')

    def test_derives_sha_from_position(self):
        host = MockHost()
        host.executive = MockExecutive(
            output='c881563d734a86f7d9cd57ac509653a61c45c240')
        pos = 'Cr-Commit-Position: refs/heads/main@{#789}'
        Carbon_commit = CarbonCommit(host, position=pos)

        self.assertEqual(Carbon_commit.position, 'refs/heads/main@{#789}')
        self.assertEqual(Carbon_commit.sha,
                         'c881563d734a86f7d9cd57ac509653a61c45c240')

    def test_derives_position_from_sha(self):
        host = MockHost()
        host.executive = mock_git_commands({
            'footers':
            'refs/heads/main@{#789}'
        })
        Carbon_commit = CarbonCommit(
            host, sha='c881563d734a86f7d9cd57ac509653a61c45c240')

        self.assertEqual(Carbon_commit.position, 'refs/heads/main@{#789}')
        self.assertEqual(Carbon_commit.sha,
                         'c881563d734a86f7d9cd57ac509653a61c45c240')

    def test_when_commit_has_no_position(self):
        host = MockHost()

        def run_command(_):
            raise ScriptError(
                'Unable to infer commit position from footers rutabaga')

        host.executive = MockExecutive(run_command_fn=run_command)
        Carbon_commit = CarbonCommit(
            host, sha='c881563d734a86f7d9cd57ac509653a61c45c240')

        self.assertEqual(Carbon_commit.position, 'no-commit-position-yet')
        self.assertEqual(Carbon_commit.sha,
                         'c881563d734a86f7d9cd57ac509653a61c45c240')

    def test_filtered_changed_files_skips_special_files(self):
        host = MockHost()

        fake_files = ['file1', 'MANIFEST.json', 'file3', 'OWNERS']
        qualified_fake_files = [RELATIVE_WPT_TESTS + f for f in fake_files]

        host.executive = mock_git_commands({
            'diff-tree':
            '\n'.join(qualified_fake_files),
            'crrev-parse':
            'c881563d734a86f7d9cd57ac509653a61c45c240',
        })

        position_footer = 'Cr-Commit-Position: refs/heads/main@{#789}'
        Carbon_commit = CarbonCommit(host, position=position_footer)

        files = Carbon_commit.filtered_changed_files()

        expected_files = ['file1', 'file3']
        qualified_expected_files = [
            RELATIVE_WPT_TESTS + f for f in expected_files
        ]

        self.assertEqual(files, qualified_expected_files)

    def test_short_sha(self):
        Carbon_commit = CarbonCommit(
            MockHost(), sha='c881563d734a86f7d9cd57ac509653a61c45c240')
        self.assertEqual(Carbon_commit.short_sha, 'c881563d73')

    def test_url(self):
        Carbon_commit = CarbonCommit(
            MockHost(), sha='c881563d734a86f7d9cd57ac509653a61c45c240')
        self.assertEqual(
            Carbon_commit.url(),
            'https://Carbon.googlesource.com/Carbon/src/+/c881563d73')
