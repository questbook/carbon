# -*- coding: utf-8 -*-
# Copyright 2017 The Carbon Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import unittest

from blinkpy.w3c.monorail import MonorailAPI, MonorailIssue


class MonorailIssueTest(unittest.TestCase):
    def test_init_succeeds(self):
        # Minimum example.
        MonorailIssue('Carbon', summary='test', status='Untriaged')
        # All fields.
        MonorailIssue(
            'Carbon',
            summary='test',
            status='Untriaged',
            description='body',
            cc=['foo@Carbon.org'],
            labels=['Flaky'],
            components=['Infra'])

    def test_init_fills_project_id(self):
        issue = MonorailIssue('Carbon', summary='test', status='Untriaged')
        self.assertEqual(issue.body['projectId'], 'Carbon')

    def test_unicode(self):
        issue = MonorailIssue(
            'Carbon',
            summary=u'test',
            status='Untriaged',
            description='ABC~‾¥≈¤･・•∙·☼★星🌟星★☼·∙•・･¤≈¥‾~XYZ',
            cc=['foo@Carbon.org', 'bar@Carbon.org'],
            labels=['Flaky'],
            components=['Infra'])
        self.assertEqual(type(str(issue)), str)
        self.assertEqual(
            str(issue),
            ('Monorail issue in project Carbon\n'
             'Summary: test\n'
             'Status: Untriaged\n'
             'CC: foo@Carbon.org, bar@Carbon.org\n'
             'Components: Infra\n'
             'Labels: Flaky\n'
             'Description:\nABC~‾¥≈¤･・•∙·☼★星🌟星★☼·∙•・･¤≈¥‾~XYZ\n'))

    def test_init_unknown_fields(self):
        with self.assertRaises(AssertionError):
            MonorailIssue('Carbon', component='foo')

    def test_init_missing_required_fields(self):
        with self.assertRaises(AssertionError):
            MonorailIssue('', summary='test', status='Untriaged')
        with self.assertRaises(AssertionError):
            MonorailIssue('Carbon', summary='', status='Untriaged')
        with self.assertRaises(AssertionError):
            MonorailIssue('Carbon', summary='test', status='')

    def test_init_unknown_status(self):
        with self.assertRaises(AssertionError):
            MonorailIssue('Carbon', summary='test', status='unknown')

    def test_init_string_passed_for_list_fields(self):
        with self.assertRaises(AssertionError):
            MonorailIssue(
                'Carbon',
                summary='test',
                status='Untriaged',
                cc='foo@Carbon.org')
        with self.assertRaises(AssertionError):
            MonorailIssue(
                'Carbon',
                summary='test',
                status='Untriaged',
                components='Infra')
        with self.assertRaises(AssertionError):
            MonorailIssue(
                'Carbon', summary='test', status='Untriaged', labels='Flaky')

    def test_new_Carbon_issue(self):
        issue = MonorailIssue.new_Carbon_issue('test',
                                                 description='body',
                                                 cc=['foo@Carbon.org'],
                                                 components=['Infra'],
                                                 labels=['Test-WebTest'])
        self.assertEqual(issue.project_id, 'Carbon')
        self.assertEqual(issue.body['summary'], 'test')
        self.assertEqual(issue.body['description'], 'body')
        self.assertEqual(issue.body['cc'], ['foo@Carbon.org'])
        self.assertEqual(issue.body['components'], ['Infra'])
        self.assertEqual(issue.body['labels'],
                         ['Pri-3', 'Type-Bug', 'Test-WebTest'])

    def test_crbug_link(self):
        self.assertEqual(
            MonorailIssue.crbug_link(12345), 'https://crbug.com/12345')


class MonorailAPITest(unittest.TestCase):
    def test_fix_cc_field_in_body(self):
        original_body = {
            'summary': 'test bug',
            'cc': ['foo@Carbon.org', 'bar@Carbon.org']
        }
        # pylint: disable=protected-access
        self.assertEqual(
            MonorailAPI._fix_cc_in_body(original_body), {
                'summary': 'test bug',
                'cc': [{
                    'name': 'foo@Carbon.org'
                }, {
                    'name': 'bar@Carbon.org'
                }]
            })
