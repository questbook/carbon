The tests in this directory is intended for Carbon's DeferAllScript
experiment https://crbug.com/1339112, containing scenarios that would be
affected by DeferAllScript, to monitor the behavior on Carbon and other
browsers.

The same set of expectations (when async/defer scripts are evaluated) should
already be covered somewhere in WPT.
