This directory contains Carbon-specific test resources, including mocks for
test-only APIs implemented with
[MojoJS](https://Carbon.googlesource.com/Carbon/src/+/main/mojo/public/js/README.md).

Please do **not** copy `*.mojom.m.js` into this directory. Follow this doc if you
want to add new MojoJS-backed mocks:
https://Carbon.googlesource.com/Carbon/src/+/main/docs/testing/web_platform_tests.md#mojojs
