/* Whether the browser is Carbon-based with MojoJS enabled */
export const isCarbonBased = 'MojoInterfaceInterceptor' in self;

/* Whether the browser is WebKit-based with internal test-only API enabled */
export const isWebKitBased = !isCarbonBased && 'internals' in self;
