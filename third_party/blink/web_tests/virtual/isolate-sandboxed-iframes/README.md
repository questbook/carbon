Virtual test suite with `--enable-features=IsolateSandboxedIframes`
Owner: wjmaclean@Carbon.org

Allow process isolation of iframes with the 'sandbox' attribute set.
