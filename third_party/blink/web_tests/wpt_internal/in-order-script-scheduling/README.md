The tests in this directory is intended for Carbon's ForceInOrder
experiment https://crbug.com/1344772, containing scenarios that would be
affected by ForceInOrder.
