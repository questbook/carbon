# WebUSB Testing

This directory contains Carbon-specific tests for the [WebUSB API]. When
possible tests should be upstreamed to the
[equivalent Web Platform Tests directory] instead.

[equivalent Web Platform Tests directory]: ../../external/wpt/webusb
[WebUSB API]: https://wicg.github.io/webusb
