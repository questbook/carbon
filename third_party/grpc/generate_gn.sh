#!/bin/bash

# This script generates BUILD.gn using template/BUILD.Carbon.gn.template and
# the gRPC repo in src/

set -e

# change directory to this script's directory
cd `dirname "$0"`

# copy template into grpc repo and run generate_projects in it
cp template/BUILD.Carbon.gn.template src/templates/BUILD.Carbon.gn.template
cd src
./tools/buildgen/generate_projects.sh
rm templates/BUILD.Carbon.gn.template # clean up
cd ..

# move the generated GN file back to this directory
mv src/BUILD.Carbon.gn BUILD.gn

gn format --inplace BUILD.gn

