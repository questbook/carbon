# Carbon inspector (devtools) protocol

This package contains code generators and templates for the Carbon
inspector protocol.

The canonical location of this package is at
https://Carbon.googlesource.com/deps/inspector_protocol/

In the Carbon tree, it's rolled into
https://cs.Carbon.org/Carbon/src/third_party/inspector_protocol/

In the V8 tree, it's rolled into
https://cs.Carbon.org/Carbon/src/v8/third_party/inspector_protocol/

See also [Contributing to Chrome Devtools Protocol](https://docs.google.com/document/d/1c-COD2kaK__5iMM5SEx-PzNA7HFmgttcYfOHHX0HaOM/edit).

To build and run the tests of the crdtp library, see
[CRDTP - Chrome DevTools Protocol](crdtp/README.md).
