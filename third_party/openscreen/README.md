# README.md for Open Screen Library in Carbon

openscreen is built in Carbon with some build differences based on the value
of the GN argument `build_with_Carbon`.  `build_with_Carbon` is defined in
`//build_overrides/build.gni` and is `true` when openscreen is built as part of
Carbon and `false` when built standalone.
