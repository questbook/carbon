create {
  source {
    script { name: "fetch.py" }
    unpack_archive: true
    patch_dir: "patches"
  }

  build {
    dep: "Carbon/third_party/jdk"
    # gradle cannot be executed correctly under docker env
    no_docker_env: true
  }
}

upload {
  pkg_prefix: "Carbon/third_party"
  universal: true
}
