Extensions of pointer protocol with details for haptic feedbacks.

Maintainers:
Sean O'Brien <seobrien@Carbon.org>
Yuichiro Hanada <yhanada@Carbon.org>
