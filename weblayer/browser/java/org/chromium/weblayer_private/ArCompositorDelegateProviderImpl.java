// Copyright 2021 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private;

import org.Carbon.base.annotations.CalledByNative;
import org.Carbon.base.annotations.JNINamespace;
import org.Carbon.components.webxr.ArCompositorDelegate;
import org.Carbon.components.webxr.ArCompositorDelegateProvider;
import org.Carbon.content_public.browser.WebContents;

/**
 * Weblayer-specific implementation of ArCompositorDelegateProvider interface.
 */
@JNINamespace("weblayer")
public class ArCompositorDelegateProviderImpl implements ArCompositorDelegateProvider {
    @CalledByNative
    public ArCompositorDelegateProviderImpl() {}

    @Override
    public ArCompositorDelegate create(WebContents webContents) {
        return new ArCompositorDelegateImpl(webContents);
    }
}
