// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private;

import org.Carbon.base.LifetimeAssert;
import org.Carbon.base.annotations.JNINamespace;
import org.Carbon.base.annotations.NativeMethods;
import org.Carbon.components.omnibox.AutocompleteSchemeClassifier;

/**
 * Creates the c++ class that provides scheme classification logic for WebLayer
 * Must call destroy() after using this object to delete the native object.
 */
@JNINamespace("weblayer")
public class AutocompleteSchemeClassifierImpl extends AutocompleteSchemeClassifier {
    private final LifetimeAssert mLifetimeAssert = LifetimeAssert.create(this);

    public AutocompleteSchemeClassifierImpl() {
        super(AutocompleteSchemeClassifierImplJni.get().createAutocompleteClassifier());
    }

    @Override
    public void destroy() {
        super.destroy();

        AutocompleteSchemeClassifierImplJni.get().deleteAutocompleteClassifier(
                super.getNativePtr());

        // If mLifetimeAssert is GC'ed before this is called, it will throw an exception
        // with a stack trace showing the stack during LifetimeAssert.create().
        LifetimeAssert.setSafeToGc(mLifetimeAssert, true);
    }

    @NativeMethods
    interface Natives {
        long createAutocompleteClassifier();
        void deleteAutocompleteClassifier(long weblayerAutocompleteSchemeClassifier);
    }
}
