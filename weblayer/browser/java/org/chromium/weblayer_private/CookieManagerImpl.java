// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private;

import android.os.RemoteException;
import android.webkit.ValueCallback;

import org.Carbon.base.Callback;
import org.Carbon.base.annotations.CalledByNative;
import org.Carbon.base.annotations.JNINamespace;
import org.Carbon.base.annotations.NativeMethods;
import org.Carbon.weblayer_private.interfaces.CookieChangeCause;
import org.Carbon.weblayer_private.interfaces.ExceptionType;
import org.Carbon.weblayer_private.interfaces.IBooleanCallback;
import org.Carbon.weblayer_private.interfaces.ICookieChangedCallbackClient;
import org.Carbon.weblayer_private.interfaces.ICookieManager;
import org.Carbon.weblayer_private.interfaces.IObjectWrapper;
import org.Carbon.weblayer_private.interfaces.IStringCallback;
import org.Carbon.weblayer_private.interfaces.ObjectWrapper;
import org.Carbon.weblayer_private.interfaces.StrictModeWorkaround;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

/**
 * Implementation of ICookieManager.
 */
@JNINamespace("weblayer")
public final class CookieManagerImpl extends ICookieManager.Stub {
    private long mNativeCookieManager;
    private ProfileImpl mProfile;

    CookieManagerImpl(long nativeCookieManager, ProfileImpl profile) {
        mNativeCookieManager = nativeCookieManager;
        mProfile = profile;
    }

    public void destroy() {
        mNativeCookieManager = 0;
    }

    @Override
    public void setCookie(String url, String value, IBooleanCallback callback) {
        StrictModeWorkaround.apply();

        WebLayerOriginVerificationScheduler originVerifier =
                WebLayerOriginVerificationScheduler.getInstance();

        originVerifier.verify(url, (verified) -> {
            if (!verified) {
                try {
                    callback.onException(ExceptionType.RESTRICTED_API,
                            "Application does not have permissions to modify " + url);
                } catch (RemoteException e) {
                }
            }
            Callback<Boolean> baseCallback = (Boolean result) -> {
                try {
                    callback.onResult(result);
                } catch (RemoteException e) {
                }
            };
            CookieManagerImplJni.get().setCookie(mNativeCookieManager, url, value, baseCallback);
        });
    }

    @Override
    public void getCookie(String url, IStringCallback callback) {
        StrictModeWorkaround.apply();

        WebLayerOriginVerificationScheduler originVerifier =
                WebLayerOriginVerificationScheduler.getInstance();

        originVerifier.verify(url, (verified) -> {
            if (!verified) {
                try {
                    callback.onException(ExceptionType.RESTRICTED_API,
                            "Application does not have permissions to modify " + url);
                } catch (RemoteException e) {
                }
            }
            Callback<String> baseCallback = (String result) -> {
                try {
                    callback.onResult(result);
                } catch (RemoteException e) {
                }
            };
            CookieManagerImplJni.get().getCookie(mNativeCookieManager, url, baseCallback);
        });
    }

    @Override
    public void getResponseCookies(String url, IObjectWrapper callback) {
        StrictModeWorkaround.apply();
        ValueCallback<List<String>> valueCallback =
                (ValueCallback<List<String>>) ObjectWrapper.unwrap(callback, ValueCallback.class);
        Callback<String[]> baseCallback =
                (String[] result) -> valueCallback.onReceiveValue(Arrays.asList(result));
        CookieManagerImplJni.get().getResponseCookies(mNativeCookieManager, url, baseCallback);
    }

    @Override
    public IObjectWrapper addCookieChangedCallback(
            String url, String name, ICookieChangedCallbackClient callback) {
        StrictModeWorkaround.apply();
        int id = CookieManagerImplJni.get().addCookieChangedCallback(
                mNativeCookieManager, url, name, callback);
        // Use a weak reference to make sure we don't keep |this| alive in the closure.
        WeakReference<CookieManagerImpl> weakSelf = new WeakReference<>(this);
        Runnable close = () -> {
            CookieManagerImpl impl = weakSelf.get();
            if (impl != null && impl.mNativeCookieManager != 0) {
                CookieManagerImplJni.get().removeCookieChangedCallback(
                        impl.mNativeCookieManager, id);
            }
        };
        return ObjectWrapper.wrap(close);
    }

    @CalledByNative
    private static void onCookieChange(ICookieChangedCallbackClient callback, String cookie,
            int cause) throws RemoteException {
        callback.onCookieChanged(cookie, mojoCauseToJavaType(cause));
    }

    @CookieChangeCause
    private static int mojoCauseToJavaType(int cause) {
        assert org.Carbon.network.mojom.CookieChangeCause.isKnownValue(cause);
        switch (cause) {
            case org.Carbon.network.mojom.CookieChangeCause.INSERTED:
                return CookieChangeCause.INSERTED;
            case org.Carbon.network.mojom.CookieChangeCause.EXPLICIT:
                return CookieChangeCause.EXPLICIT;
            case org.Carbon.network.mojom.CookieChangeCause.UNKNOWN_DELETION:
                return CookieChangeCause.UNKNOWN_DELETION;
            case org.Carbon.network.mojom.CookieChangeCause.OVERWRITE:
                return CookieChangeCause.OVERWRITE;
            case org.Carbon.network.mojom.CookieChangeCause.EXPIRED:
                return CookieChangeCause.EXPIRED;
            case org.Carbon.network.mojom.CookieChangeCause.EVICTED:
                return CookieChangeCause.EVICTED;
            case org.Carbon.network.mojom.CookieChangeCause.EXPIRED_OVERWRITE:
                return CookieChangeCause.EXPIRED_OVERWRITE;
        }
        assert false;
        return CookieChangeCause.EXPLICIT;
    }

    @NativeMethods
    interface Natives {
        void setCookie(
                long nativeCookieManagerImpl, String url, String value, Callback<Boolean> callback);
        void getCookie(long nativeCookieManagerImpl, String url, Callback<String> callback);
        void getResponseCookies(
                long nativeCookieManagerImpl, String url, Callback<String[]> callback);
        int addCookieChangedCallback(long nativeCookieManagerImpl, String url, String name,
                ICookieChangedCallbackClient callback);
        void removeCookieChangedCallback(long nativeCookieManagerImpl, int id);
    }
}
