// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private;

import org.Carbon.base.annotations.CalledByNative;
import org.Carbon.blink.mojom.Authenticator;
import org.Carbon.components.webauthn.AuthenticatorFactory;
import org.Carbon.content_public.browser.InterfaceRegistrar;
import org.Carbon.content_public.browser.RenderFrameHost;
import org.Carbon.content_public.browser.WebContents;
import org.Carbon.installedapp.mojom.InstalledAppProvider;
import org.Carbon.payments.mojom.PaymentRequest;
import org.Carbon.services.service_manager.InterfaceRegistry;
import org.Carbon.weblayer_private.payments.WebLayerPaymentRequestFactory;
import org.Carbon.webshare.mojom.ShareService;

/**
 * Registers Java implementations of mojo interfaces.
 */
class MojoInterfaceRegistrar {
    @CalledByNative
    private static void registerMojoInterfaces() {
        InterfaceRegistrar.Registry.addWebContentsRegistrar(new WebContentsInterfaceRegistrar());
        InterfaceRegistrar.Registry.addRenderFrameHostRegistrar(
                new RenderFrameHostInterfaceRegistrar());
    }

    private static class WebContentsInterfaceRegistrar implements InterfaceRegistrar<WebContents> {
        @Override
        public void registerInterfaces(InterfaceRegistry registry, final WebContents webContents) {
            registry.addInterface(ShareService.MANAGER, new WebShareServiceFactory(webContents));
        }
    }

    private static class RenderFrameHostInterfaceRegistrar
            implements InterfaceRegistrar<RenderFrameHost> {
        @Override
        public void registerInterfaces(
                InterfaceRegistry registry, final RenderFrameHost renderFrameHost) {
            registry.addInterface(Authenticator.MANAGER, new AuthenticatorFactory(renderFrameHost));
            registry.addInterface(
                    InstalledAppProvider.MANAGER, new InstalledAppProviderFactory(renderFrameHost));
            registry.addInterface(
                    PaymentRequest.MANAGER, new WebLayerPaymentRequestFactory(renderFrameHost));
        }
    }
}
