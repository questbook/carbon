// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private;

import org.Carbon.base.annotations.CalledByNative;
import org.Carbon.base.annotations.JNINamespace;

/**
 * A helper class to determine if an exception is relevant to WebLayer. Called if an uncaught
 * exception is detected.
 */
@JNINamespace("weblayer")
public final class WebLayerExceptionFilter {
    // The filename prefix used by Carbon proguarding, which we use to
    // recognise stack frames that reference WebLayer.
    private static final String Carbon_PREFIX = "Carbon-";

    @CalledByNative
    private static boolean stackTraceContainsWebLayerCode(Throwable t) {
        for (StackTraceElement frame : t.getStackTrace()) {
            if (frame.getClassName().startsWith("org.Carbon.")
                    || (frame.getFileName() != null
                            && frame.getFileName().startsWith(Carbon_PREFIX))) {
                return true;
            }
        }
        return false;
    }
}
