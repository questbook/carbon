// Copyright 2021 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private.bluetooth;

import org.Carbon.base.annotations.CalledByNative;
import org.Carbon.base.annotations.JNINamespace;
import org.Carbon.components.omnibox.AutocompleteSchemeClassifier;
import org.Carbon.components.permissions.BluetoothScanningPromptAndroidDelegate;
import org.Carbon.weblayer_private.AutocompleteSchemeClassifierImpl;

/**
 *  The implementation of {@link BluetoothScanningPromptAndroidDelegate} for WebLayer.
 */
@JNINamespace("weblayer")
public class WebLayerBluetoothScanningPromptAndroidDelegate
        implements BluetoothScanningPromptAndroidDelegate {
    /**
     * {@inheritDoc}
     */
    @Override
    public AutocompleteSchemeClassifier createAutocompleteSchemeClassifier() {
        return new AutocompleteSchemeClassifierImpl();
    }

    @CalledByNative
    private static WebLayerBluetoothScanningPromptAndroidDelegate create() {
        return new WebLayerBluetoothScanningPromptAndroidDelegate();
    }
}
