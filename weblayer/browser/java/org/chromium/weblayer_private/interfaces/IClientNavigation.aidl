// Copyright 2019 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private.interfaces;

/**
 * Represents a navigation on the *client* side.
 */
interface IClientNavigation {
}
