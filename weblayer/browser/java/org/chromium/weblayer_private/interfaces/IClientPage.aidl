// Copyright 2021 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private.interfaces;

/**
 * Represents a page on the *client* side.
 */
interface IClientPage {
}
