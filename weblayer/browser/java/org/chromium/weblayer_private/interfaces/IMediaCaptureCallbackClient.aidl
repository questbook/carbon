// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private.interfaces;

import org.Carbon.weblayer_private.interfaces.IObjectWrapper;

interface IMediaCaptureCallbackClient {
  void onMediaCaptureRequested(boolean audio, boolean video, in IObjectWrapper requestResult) = 0;
  void onMediaCaptureStateChanged(boolean audio, boolean video) = 1;
}
