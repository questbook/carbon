// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private.interfaces;

// Added in Version 88.
interface IProfileClient {
  void onProfileDestroyed() = 0;
}
