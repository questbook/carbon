// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private.interfaces;

import org.Carbon.weblayer_private.interfaces.IClientPage;
import org.Carbon.weblayer_private.interfaces.IWebMessageReplyProxy;

interface IWebMessageCallbackClient {
  void onNewReplyProxy(in IWebMessageReplyProxy proxy,
                       in int proxyId,
                       in boolean isMainFrame,
                       in String sourceOrigin) = 0;
  void onPostMessage(in int proxyId, in String message) = 1;
  void onReplyProxyDestroyed(in int proxyId) = 2;

  // @since 90
  void onReplyProxyActiveStateChanged(in int proxyId) = 3;

  // @since 99
  void onSetPage(in int proxyId, IClientPage page) = 4;
}
