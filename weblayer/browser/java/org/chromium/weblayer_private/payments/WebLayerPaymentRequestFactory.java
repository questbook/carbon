// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private.payments;

import androidx.annotation.Nullable;

import org.Carbon.components.payments.BrowserPaymentRequest;
import org.Carbon.components.payments.InvalidPaymentRequest;
import org.Carbon.components.payments.MojoPaymentRequestGateKeeper;
import org.Carbon.components.payments.OriginSecurityChecker;
import org.Carbon.components.payments.PaymentAppServiceBridge;
import org.Carbon.components.payments.PaymentFeatureList;
import org.Carbon.components.payments.PaymentRequestService;
import org.Carbon.components.payments.PaymentRequestServiceUtil;
import org.Carbon.components.payments.PrefsStrings;
import org.Carbon.components.payments.SslValidityChecker;
import org.Carbon.components.user_prefs.UserPrefs;
import org.Carbon.content_public.browser.BrowserContextHandle;
import org.Carbon.content_public.browser.PermissionsPolicyFeature;
import org.Carbon.content_public.browser.RenderFrameHost;
import org.Carbon.content_public.browser.WebContents;
import org.Carbon.content_public.browser.WebContentsStatics;
import org.Carbon.payments.mojom.PaymentRequest;
import org.Carbon.services.service_manager.InterfaceFactory;
import org.Carbon.url.GURL;
import org.Carbon.weblayer_private.ProfileImpl;
import org.Carbon.weblayer_private.TabImpl;

/** Creates an instance of PaymentRequest for use in WebLayer. */
public class WebLayerPaymentRequestFactory implements InterfaceFactory<PaymentRequest> {
    private final RenderFrameHost mRenderFrameHost;

    /**
     * Production implementation of the WebLayerPaymentRequestService's Delegate. Gives true answers
     * about the system.
     */
    private static class WebLayerPaymentRequestDelegateImpl
            implements PaymentRequestService.Delegate {
        private final RenderFrameHost mRenderFrameHost;

        /* package */ WebLayerPaymentRequestDelegateImpl(RenderFrameHost renderFrameHost) {
            mRenderFrameHost = renderFrameHost;
        }

        @Override
        public BrowserPaymentRequest createBrowserPaymentRequest(
                PaymentRequestService paymentRequestService) {
            return new WebLayerPaymentRequestService(paymentRequestService, this);
        }

        @Override
        public boolean isOffTheRecord() {
            ProfileImpl profile = getProfile();
            if (profile == null) return true;
            return profile.isIncognito();
        }

        @Override
        public String getInvalidSslCertificateErrorMessage() {
            WebContents webContents =
                    PaymentRequestServiceUtil.getLiveWebContents(mRenderFrameHost);
            if (webContents == null || webContents.isDestroyed()) return null;

            GURL url = webContents.getLastCommittedUrl();
            if (url == null || !OriginSecurityChecker.isSchemeCryptographic(url)) {
                return null;
            }
            return SslValidityChecker.getInvalidSslCertificateErrorMessage(webContents);
        }

        @Override
        public boolean prefsCanMakePayment() {
            BrowserContextHandle profile = getProfile();
            return profile != null
                    && UserPrefs.get(profile).getBoolean(PrefsStrings.CAN_MAKE_PAYMENT_ENABLED);
        }

        @Nullable
        @Override
        public String getTwaPackageName() {
            return null;
        }

        @Nullable
        private ProfileImpl getProfile() {
            WebContents webContents =
                    PaymentRequestServiceUtil.getLiveWebContents(mRenderFrameHost);
            if (webContents == null) return null;
            TabImpl tab = TabImpl.fromWebContents(webContents);
            if (tab == null) return null;
            return tab.getProfile();
        }
    }

    /**
     * Creates an instance of WebLayerPaymentRequestFactory.
     * @param renderFrameHost The frame that issues the payment request on the merchant page.
     */
    public WebLayerPaymentRequestFactory(RenderFrameHost renderFrameHost) {
        mRenderFrameHost = renderFrameHost;
    }

    @Override
    public PaymentRequest createImpl() {
        if (mRenderFrameHost == null) return new InvalidPaymentRequest();
        if (!mRenderFrameHost.isFeatureEnabled(PermissionsPolicyFeature.PAYMENT)) {
            mRenderFrameHost.terminateRendererDueToBadMessage(241 /*PAYMENTS_WITHOUT_PERMISSION*/);
            return null;
        }

        if (!PaymentFeatureList.isEnabled(PaymentFeatureList.WEB_PAYMENTS)) {
            return new InvalidPaymentRequest();
        }

        PaymentRequestService.Delegate delegate =
                new WebLayerPaymentRequestDelegateImpl(mRenderFrameHost);

        WebContents webContents = WebContentsStatics.fromRenderFrameHost(mRenderFrameHost);
        if (webContents == null || webContents.isDestroyed()) return new InvalidPaymentRequest();
        return new MojoPaymentRequestGateKeeper(
                (client, onClosed)
                        -> new PaymentRequestService(mRenderFrameHost, client, onClosed, delegate,
                                PaymentAppServiceBridge::new));
    }
}
