// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer_private.test;

import androidx.annotation.VisibleForTesting;

import org.Carbon.base.annotations.CalledByNative;
import org.Carbon.base.annotations.JNINamespace;
import org.Carbon.base.annotations.NativeMethods;
import org.Carbon.components.infobars.InfoBar;
import org.Carbon.components.infobars.InfoBarCompactLayout;
import org.Carbon.content_public.browser.WebContents;
import org.Carbon.weblayer_private.TabImpl;

/**
 * A test infobar.
 */
@JNINamespace("weblayer")
public class TestInfoBar extends InfoBar {
    @VisibleForTesting
    public TestInfoBar() {
        super(0, 0, null, null);
    }

    @Override
    protected boolean usesCompactLayout() {
        return true;
    }

    @Override
    protected void createCompactLayoutContent(InfoBarCompactLayout layout) {
        new InfoBarCompactLayout.MessageBuilder(layout)
                .withText("I am a compact infobar")
                .buildAndInsert();
    }

    @CalledByNative
    private static TestInfoBar create() {
        return new TestInfoBar();
    }

    public static void show(TabImpl tab) {
        TestInfoBarJni.get().show(tab.getWebContents());
    }

    @NativeMethods
    interface Natives {
        void show(WebContents webContents);
    }
}
