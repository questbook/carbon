// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import org.Carbon.webengine.interfaces.IBooleanCallback;
import org.Carbon.webengine.interfaces.IStringCallback;

oneway interface ICookieManagerDelegate {
    void setCookie(String uri, String value, IBooleanCallback callback) = 1;
    void getCookie(String uri, IStringCallback callback) = 2;
}
