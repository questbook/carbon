// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import android.net.Uri;

import org.Carbon.webengine.interfaces.ITabProxy;
import org.Carbon.webengine.interfaces.ITabNavigationControllerProxy;

parcelable INavigationParams {
    Uri uri;
    int statusCode;
    boolean isSameDocument;
}