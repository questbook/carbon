// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import org.Carbon.webengine.interfaces.ITabParams;

oneway interface ITabCallback {
    void onResult(in ITabParams tabParams) = 1;
}
