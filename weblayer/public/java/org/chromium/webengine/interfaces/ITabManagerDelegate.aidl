// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import org.Carbon.webengine.interfaces.ITabProxy;
import org.Carbon.webengine.interfaces.ITabCallback;
import org.Carbon.webengine.interfaces.ITabListObserverDelegate;

oneway interface ITabManagerDelegate {

    void setTabListObserverDelegate(ITabListObserverDelegate tabListObserverDelegate) = 1;
    void notifyInitialTabs() = 2;

    void getActiveTab(ITabCallback callback) = 3;
    void createTab(ITabCallback callback) = 4;
}