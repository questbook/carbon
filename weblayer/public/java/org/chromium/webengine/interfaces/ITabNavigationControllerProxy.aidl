// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import org.Carbon.webengine.interfaces.IBooleanCallback;
import org.Carbon.webengine.interfaces.INavigationObserverDelegate;

oneway interface ITabNavigationControllerProxy {
    void navigate(in String uri) = 1;
    void goBack() = 2;
    void goForward() = 3;
    void canGoBack(IBooleanCallback callback) = 4;
    void canGoForward(IBooleanCallback callback) = 5;

    void setNavigationObserverDelegate(INavigationObserverDelegate tabNavigationDelegate) = 6;
}