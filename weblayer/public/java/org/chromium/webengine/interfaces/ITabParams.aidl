// Copyright 2021 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import org.Carbon.webengine.interfaces.ITabProxy;
import org.Carbon.webengine.interfaces.ITabNavigationControllerProxy;

parcelable ITabParams {
    ITabProxy tabProxy;
    String tabGuid;
    String uri;
    ITabNavigationControllerProxy navigationControllerProxy;
}