// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import org.Carbon.webengine.interfaces.IBooleanCallback;
import org.Carbon.webengine.interfaces.ITabListObserverDelegate;

oneway interface IWebEngineDelegate {
    void shutdown() = 1;
    void tryNavigateBack(IBooleanCallback callback) = 2;
}
