// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import org.Carbon.webengine.interfaces.ICookieManagerDelegate;
import org.Carbon.webengine.interfaces.ITabManagerDelegate;
import org.Carbon.webengine.interfaces.IWebEngineDelegate;
import org.Carbon.webengine.interfaces.IWebFragmentEventsDelegate;

oneway interface IWebEngineDelegateClient {
    void onDelegatesReady(
            IWebEngineDelegate delegate,
            IWebFragmentEventsDelegate fragmentEventsDelegate,
            ITabManagerDelegate tabManagerDelegate,
            ICookieManagerDelegate cookieManagerDelegate) = 1;
}
