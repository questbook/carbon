// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

parcelable IWebEngineParams {
    String profileName;
    String persistenceId;
    boolean isIncognito;
}
