// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

oneway interface IWebMessageReplyProxy {
    void postMessage(String message) = 1;
}
