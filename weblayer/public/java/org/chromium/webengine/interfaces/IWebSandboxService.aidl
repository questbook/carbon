// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.webengine.interfaces;

import org.Carbon.webengine.interfaces.IBooleanCallback;
import org.Carbon.webengine.interfaces.IStringCallback;
import org.Carbon.webengine.interfaces.IWebEngineParams;
import org.Carbon.webengine.interfaces.IWebEngineDelegateClient;
import org.Carbon.webengine.interfaces.IWebSandboxCallback;

oneway interface IWebSandboxService {
    void isAvailable(IBooleanCallback callback) = 1;
    void getVersion(IStringCallback callback) = 2;
    void getProviderPackageName(IStringCallback callback) = 3;

    void initializeBrowserProcess(in IWebSandboxCallback callback) = 4;
    void createWebEngineDelegate(in IWebEngineParams params, IWebEngineDelegateClient fragmentClient) = 5;

    void setRemoteDebuggingEnabled(in boolean enabled) = 6;
}
