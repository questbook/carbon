// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @hide
 */
@IntDef({DownloadError.NO_ERROR, DownloadError.SERVER_ERROR, DownloadError.SSL_ERROR,
        DownloadError.CONNECTIVITY_ERROR, DownloadError.NO_SPACE, DownloadError.FILE_ERROR,
        DownloadError.CANCELLED, DownloadError.OTHER_ERROR})
@Retention(RetentionPolicy.SOURCE)
@interface DownloadError {
    int NO_ERROR = org.Carbon.weblayer_private.interfaces.DownloadError.NO_ERROR;
    int SERVER_ERROR = org.Carbon.weblayer_private.interfaces.DownloadError.SERVER_ERROR;
    int SSL_ERROR = org.Carbon.weblayer_private.interfaces.DownloadError.SSL_ERROR;
    int CONNECTIVITY_ERROR =
            org.Carbon.weblayer_private.interfaces.DownloadError.CONNECTIVITY_ERROR;
    int NO_SPACE = org.Carbon.weblayer_private.interfaces.DownloadError.NO_SPACE;
    int FILE_ERROR = org.Carbon.weblayer_private.interfaces.DownloadError.FILE_ERROR;
    int CANCELLED = org.Carbon.weblayer_private.interfaces.DownloadError.CANCELLED;
    int OTHER_ERROR = org.Carbon.weblayer_private.interfaces.DownloadError.OTHER_ERROR;
}
