// Copyright 2022 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer;

import org.Carbon.weblayer_private.interfaces.ExceptionType;

class ExceptionHelper {
    static @ExceptionType int convertType(@ExceptionType int type) {
        switch (type) {
            case ExceptionType.RESTRICTED_API:
                return org.Carbon.webengine.interfaces.ExceptionType.RESTRICTED_API;
            case ExceptionType.UNKNOWN:
                return org.Carbon.webengine.interfaces.ExceptionType.UNKNOWN;
        }
        assert false : "Unexpected ExceptionType: " + String.valueOf(type);
        return org.Carbon.webengine.interfaces.ExceptionType.UNKNOWN;
    }
}
