// Copyright 2021 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({ExternalIntentInIncognitoUserDecision.ALLOW, ExternalIntentInIncognitoUserDecision.DENY})
@Retention(RetentionPolicy.SOURCE)
@interface ExternalIntentInIncognitoUserDecision {
    int ALLOW =
            org.Carbon.weblayer_private.interfaces.ExternalIntentInIncognitoUserDecision.ALLOW;
    int DENY = org.Carbon.weblayer_private.interfaces.ExternalIntentInIncognitoUserDecision.DENY;
}
