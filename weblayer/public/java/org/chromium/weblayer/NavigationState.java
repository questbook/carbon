// Copyright 2019 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @hide
 */
@IntDef({NavigationState.WAITING_RESPONSE, NavigationState.RECEIVING_BYTES,
        NavigationState.COMPLETE, NavigationState.FAILED})
@Retention(RetentionPolicy.SOURCE)
@interface NavigationState {
    int WAITING_RESPONSE =
            org.Carbon.weblayer_private.interfaces.NavigationState.WAITING_RESPONSE;
    int RECEIVING_BYTES = org.Carbon.weblayer_private.interfaces.NavigationState.RECEIVING_BYTES;
    int COMPLETE = org.Carbon.weblayer_private.interfaces.NavigationState.COMPLETE;
    int FAILED = org.Carbon.weblayer_private.interfaces.NavigationState.FAILED;
}
