// Copyright 2020 The Carbon Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.Carbon.weblayer;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import androidx.test.filters.SmallTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.Carbon.base.test.BaseJUnit4ClassRunner;

/**
 * Tests for (@link WebViewCompatibilityHelper}.
 */
@RunWith(BaseJUnit4ClassRunner.class)
public class WebViewCompatibilityHelperTest {
    @Test
    @SmallTest
    public void testLibraryPaths() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        ClassLoader classLoader = WebViewCompatibilityHelper.initialize(appContext);
        String[] libraryPaths = WebViewCompatibilityHelper.getLibraryPaths(classLoader);
        for (String path : libraryPaths) {
            Assert.assertTrue(path.startsWith("/./"));
        }
    }
}
